# zabbix-debian-updates

Original from [Monitoring Debian updates](https://www.zabbix.com/forum/zabbix-cookbook/18892-monitoring-debian-updates)

Check for Debian updates and security updates. On value different from 0, a trigger is activated.

## Installation

Copy the file debian-updates.conf to the zabbix-agent conf directory:

	cp -v zabbix_agentd.d/debian-updates.conf /etc/zabbix/zabbix_agentd.conf.d/

In zabbix import the template `debian_updates.xml`
